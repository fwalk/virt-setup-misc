# Notes

Ssh into VM

    ssh -i ~/.ssh/rpi3vm_ed25519 -p 5555 pi@127.0.0.1

... or 

    ssh rpi3vm

... with ~/.ssh/config

    Host rpi3vm
            Hostname 127.0.0.1
            Port 5555
            User pi
            IdentityFile ~/.ssh/rpi3vm_ed25519
            GlobalKnownHostsFile /dev/null
            UserKnownHostsFile /dev/null


Stop emulator processes

    pkill -f qemu-system-aarch64

Systemd set default-target

    systemctl set-default -f multi-user.target

... or via kernel parameter

    systemd.unit=multi-user.target

Replace 'pip' with 'pip3'

    alias pip=pip3

Locale setting

    export LC_ALL=C

Python Virtual Env

    python3 -m venv v123

Pypi mirror

    pip install python-pypi-mirror

    pypi-mirror download -d downloads bcrypt

BCrypt dependencies

    apt-get install build-essential libffi-dev python-dev

Rsync using non-standard port

    rsync -arvz -e 'ssh -p 5555 -i ~/.ssh/rpi3vm_ed25519' --progress pi@127.0.0.1:~/downloads downloads