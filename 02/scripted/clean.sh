#!/usr/bin/bash

WORKDIR=${WORKDIR:-$(pwd)}

echo "=== Cleanup"

set -x
cd $WORKDIR

rm -f kernel*.img
rm -f bcm*rpi*.dtb
rm -f rpi*.img
rm -f *-raspios*armhf*.img
rm -f sshd_config
rm -f nohup.out
