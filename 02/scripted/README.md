# Raspberry PI 3 Virtual Machine within QEMU

For details refer to: [1]

## Quickstart

Install:

    ./clean.sh
    ./run.sh
    timeout 300 tail -f nohup.out

Connect:

    ssh -i ~/.ssh/rpi3vm_ed25519 -p 5555 pi@127.0.0.1

## Preparation

QEMU >= 5.1 needed

    $ qemu-system-aarch64 --version
    QEMU emulator version 5.1.0 (qemu-5.1.0-8.fc33)

    qemu-system-aarch64 -M help | grep -i rasp
    raspi2               Raspberry Pi 2B
    raspi3               Raspberry Pi 3B

    $qemu-system-aarch64 -cpu help | grep 53
    cortex-a53

## Execution via QEMU

[link](./run.sh)

    qemu-system-aarch64 \
        -m 1024 \
        -M raspi3 \
        -kernel ./kernel/kernel8.img \
        -dtb ./dtb/bcm2710-rpi-3-b-plus.dtb \
        -sd 2020-12-02-raspios-buster-armhf-full.img \
        -append "console=ttyAMA0 root=/dev/mmcblk0p2 rw rootwait rootfstype=ext4" \
        -nographic \
        -device usb-net,netdev=net0 \
        -netdev user,id=net0,hostfwd=tcp::5555-:22


## Execution via Libvirt

Unfortunatelly, did not work yet.

    $ virsh create qemu-raspi-3bp.xml 
    error: Failed to create domain from qemu-raspi-3bp.xml
    error: unsupported configuration: ACPI requires UEFI on this architecture

The xml has been generated with this command accordingly to [4]:

    sudo virt-install \
    --connect qemu:///system \
    --name rpi3 \
    --memory 2048 \
    --vcpus 4 \
    --import \
    --boot \
        "dtb=/var/lib/libvirt/misc/bcm2710-rpi-3-b-plus.dtb,\
    kernel=/var/lib/libvirt/misc/kernel8.img,\
    kernel_args=rw earlyprintk loglevel=8 console=ttyAMA0,115200 dwc_otg.lpm_enable=0 root=/dev/mmcblk0p2 rootdelay=1" \
    --os-variant debian10 \
    --disk "/var/lib/libvirt/images/2020-12-02-raspios-buster-armhf-full.img,bus=virtio" \
    --video qxl \
    --virt-type qemu \
    --arch aarch64 \
    --machine raspi3 \
    --print-xml \
    --check path_in_use=off

## Stop QEMU process

Example:

    pkill -f qemu-system-aarch64

# References

- [1] https://github.com/dhruvvyas90/qemu-rpi-kernel/tree/master/native-emuation
- [2] https://fedoraproject.org/wiki/QA:Testcase_Virt_AArch64_on_x86
- [3] https://stackoverflow.com/questions/61562014/qemu-kernel-for-raspberry-pi-3-with-networking-and-virtio-support
- [4] https://stackoverflow.com/questions/64698385/libvirt-qemu-system-aarch64-property-pflash0-not-found
- [5] https://libguestfs.org/guestfish.1.html
