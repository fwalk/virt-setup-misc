#!/usr/bin/bash

set -eo pipefail
[ ! -z "$DEBUG" ] && set -x

RPI_OS_DIST=2020-12-02-raspios-buster-armhf-full
RPI_OS_DOWNLOAD=http://downloads.raspberrypi.org/raspios_full_armhf/images/raspios_full_armhf-2020-12-04
RPI_VM=rpi3bp
RPI_KERNEL=kernel8.img
RPI_DTB=bcm2710-rpi-3-b-plus.dtb
RPI_FS=rpi3bp.img
RPI_SSHFWD=127.0.0.1:5555
RPI_SSHID=${HOME}/.ssh/rpi3vm_ed25519
SW_ARCHIV=/data/user/${USER}

echo "=== Build VM: $RPI_VM"

WORKDIR=${WORKDIR:-$(pwd)}
cd $WORKDIR

echo "--- Workdir: $(pwd)"

RPI_SSHID_DIR=$(dirname ${RPI_SSHID})
RPI_SSHID_BN=$(basename ${RPI_SSHID})

if [ ! -f $RPI_FS ] ; then
    echo "--- Obtain image ..."
    if [ -f ${SW_ARCHIV}/${RPI_OS_DIST}.zip ] ; then
        cp -v ${SW_ARCHIV}/${RPI_OS_DIST}.zip .
    else
        curl -OL ${RPI_OS_DOWNLOAD}/${RPI_OS_DIST}.zip
    fi
    unzip ${RPI_OS_DIST}.zip
    mv ${RPI_OS_DIST}.img ${RPI_FS}
    [ -f /data/user/${USER}/${RPI_OS_DIST}.zip ] && rm ${RPI_OS_DIST}.zip
fi

if [ ! -f $RPI_SSHID ] ; then
    echo "--- Generate SSH key ..."
    if [ ! -d ${RPI_SSHID_DIR} ] ; then 
        mkdir $RPI_SSHID_DIR
        chmod 700 $RPI_SSHID_DIR
    fi
    ssh-keygen -t ed25519 -f ${RPI_SSHID} -C "pi@${RPI_VM}" -N ""
fi

echo "--- Copy-out from image ..."
rm -f ${RPI_DTB} ${RPI_KERNEL} sshd_config

guestfish <<EOF
add ${RPI_FS}
run

mount /dev/sda1 /
copy-out /${RPI_DTB} ${WORKDIR}
copy-out /${RPI_KERNEL} ${WORKDIR}
umount-all

mount /dev/sda2 /
copy-out /etc/ssh/sshd_config ${WORKDIR}
umount-all
EOF

sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/g' sshd_config
sed -i 's/UsePAM yes/UsePAM no/g' sshd_config
sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/g' sshd_config

echo "--- Prepare image ..."
guestfish <<EOF
add ${RPI_FS}
run

mount /dev/sda1 /
write /ssh ""
umount-all

mount /dev/sda2 /
mkdir-p /home/pi/.ssh/
copy-in ${RPI_SSHID_DIR}/${RPI_SSHID_BN}.pub /home/pi/.ssh/
mv /home/pi/.ssh/${RPI_SSHID_BN}.pub /home/pi/.ssh/authorized_keys
chown 1000 1000 /home/pi/.ssh
chmod 0700 /home/pi/.ssh/
chown 1000 1000 /home/pi/.ssh/authorized_keys
chmod 0600 /home/pi/.ssh/authorized_keys
rm /etc/ssh/sshd_config
copy-in ${WORKDIR}/sshd_config /etc/ssh/
chmod 0600 /etc/ssh/sshd_config
umount-all
EOF

echo "Resize image ..."
qemu-img resize -f raw -q ${RPI_FS} 16G

echo "Start VM ..."
nohup qemu-system-aarch64 \
    -M raspi3 -m 1G \
    -dtb ${RPI_DTB} \
    -kernel ${RPI_KERNEL} \
    -drive file=${RPI_FS},format=raw,if=sd \
    -append "rw earlycon=pl011,0x3f201000 console=ttyAMA0 loglevel=8 root=/dev/mmcblk0p2 rootwait rootfstype=ext4 fsck.repair=yes memtest=1" \
    -nographic \
    -device usb-net,netdev=net0 \
    -netdev user,id=net0,hostfwd=tcp:${RPI_SSHFWD}-:22 \
    &

echo "Done
....................................................................................
Connect via ssh:      ${RPI_SSHFWD}, hostname: $(hostname), id: ${RPI_SSHID}
Stop session with:    pkill -f qemu-system-aarch64
Check console at:     ${WORKDIR}/nohup.out
...................................................................................."
