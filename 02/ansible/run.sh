#!/usr/bin/bash

banner Provisioning

ansible-playbook rpi3vm-provisioning.yml

exit

banner Update

ansible-playbook rpi3vm-update.yml

exit

banner pypi-mirror

ansible-playbook rpi3vm-pypi-mirror.yml

banner mirror-example

ansible-playbook rpi3vm-pypi-mirror.yml --tags never
