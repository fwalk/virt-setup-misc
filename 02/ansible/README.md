# Raspberry PI 3 VM within QEMU


## Create VMs

    ansible-playbook rpi3vm-provisioning.yml

## Update OS

    ansible-playbook rpi3vm-update.yml

## Deploy Pypi Mirror

    ansible-playbook rpi3vm-pypi-mirror.yml

Run afterwards with tag "never" to mirror bycrypt as complex and somehow timeconsuming example

    --tags never

## Site note

Timestamps on console output using **ts** command:

    bash -c 'ansible-playbook rpi3vm-pypi-mirror.yml | ts "[%Y-%m-%d %H:%M:%S]"; exit "${PIPESTATUS[0]}"'
