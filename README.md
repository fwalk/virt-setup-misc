
# VM Setup Examples

| Directory                      | Description                           |
|--------------------------------|---------------------------------------|
| [01](./01)                     | QEMU, Raspi2, Raspberry OS, scripted  |
| [02/scripted](./02/scripted)   | QEMU, Raspi3, Raspberry OS, scripted  |
| [02/ansible](./02/ansible)     | QEMU, Raspi3, Raspberry OS, Ansible   |
| [03](./03)                     | Libvirt, x64, Ubuntu, Ansible module  |


