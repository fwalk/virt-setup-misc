#!/usr/bin/bash

set -eo pipefail
set -x

export RPI_KERNEL=kernel-qemu-4.4.34-jessie
export RPI_DIST=2017-03-02-raspbian-jessie
export RPI_DL_BASE=raspbian-2017-03-03

export RPI_FS=${RPI_DIST}.img
export QEMU=$(which qemu-system-arm)

# Special Qemu Kernel

if [ ! -f $RPI_KERNEL ] ; then
    curl -OL https://github.com/dhruvvyas90/qemu-rpi-kernel/raw/master/$RPI_KERNEL
fi

# Raspberry PI OS

if [ ! -f ${RPI_DIST}.zip ] ; then
    curl \
        -o ${RPI_DIST}.zip \
        -L http://downloads.raspberrypi.org/raspbian/images/${RPI_DL_BASE}/${RPI_DIST}.zip
fi

[ ! -f ${RPI_DIST}.img ] && unzip ${RPI_DIST}.zip

if [ ! -f _image_prepared ] ; then
    qemu-img resize ${RPI_DIST}.img 16G
    if [ -d tmpmnt ] ; then 
        sudo umount -f -d -v tmpmnt || true
    fi
    mkdir -p tmpmnt
    SECTOR1=$( fdisk -l $RPI_FS | grep FAT32 | awk '{ print $2 }' )
    SECTOR2=$( fdisk -l $RPI_FS | grep Linux | awk '{ print $2 }' )
    OFFSET1=$(( SECTOR1 * 512 ))
    OFFSET2=$(( SECTOR2 * 512 ))

    sudo -s <<EOF1
set -x
mount $RPI_FS -o offset=$OFFSET1 tmpmnt
touch tmpmnt/ssh
ls -lad tmpmnt/ssh
sleep 3
umount tmpmnt
EOF1

    sudo -s <<EOF2
set -x
mount $RPI_FS -o offset=$OFFSET2 tmpmnt
cat > tmpmnt/etc/udev/rules.d/90-qemu.rules <<EOF3
KERNEL=="sda", SYMLINK+="mmcblk0"
KERNEL=="sda?", SYMLINK+="mmcblk0p%n"
KERNEL=="sda2", SYMLINK+="root"
EOF3
umount tmpmnt
EOF2

    touch _image_prepared
fi

$QEMU \
  -kernel $RPI_KERNEL \
  -cpu arm1176 \
  -m 256 \
  -M versatilepb \
  -no-reboot \
  -serial stdio \
  -append "root=/dev/sda2 panic=1 rootfstype=ext4 rw init=/bin/bash" \
  -drive "file=$RPI_FS,index=0,media=disk,format=raw"
