# VM setup via Ansible Libvirt-Virt module

## Setup

Install the community libvirt module:

    ansible-galaxy collection install community.libvirt

Install a role which uses module installed above. Due to missing meta-information this is currently not possible with an ansible-galaxy requirements file.

    mkdir roles
    git clone https://github.com/noahbailey/ansible-qemu-kvm roles/ansible-qemu-kvm

Run the playbook:

    ansible-playbook vm-play.yaml --ask-become-pass

Verify with virsh

    $ virsh --connect qemu:///system list --all
    Id   Name          State
    -----------------------------
    1    u18-svr-001   running

## References

- [1] https://docs.ansible.com/ansible/latest/collections/community/libvirt/virt_module.html
- [2] https://github.com/noahbailey/ansible-qemu-kvm


